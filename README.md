# Content Metada Services - Starz Assignment

## Compile

1. Download Git
2. Navigate to download ubication and run mvn clean install

## Execution

1. Download Git 
2. Navigate to download ubication where content-metadata-services-0.0.1-SNAPSHOT.jar is
3. Execute:  java -jar content-metadata-services-0.0.1-SNAPSHOT.jar

or 

1. Download Git 
2. Compile project
3. Navigate to download ubication and to the folder \target
4. Execute:  java -jar content-metadata-services-0.0.1-SNAPSHOT.jar
5. Open a Web Navigator and access url: http://localhost:8080/media?filter=censoring&level=censored
  			
			
## Frameworks used
  	
### Spring boot

	Spring boot to easily deploy the application. 

###	Spring Actuator

	Monitoring the state of the application.  

###	Spring Devtools

	Make it easy to develop.

###	Spring-Netflix Hystrix
	
	Make the application fault tolerant.
	
###	Spring-Netflix Feign
	
	Easy declararive Rest Client.

###	Apache Commons
	
	Cleaner code.
	
### Test frameworks
	
	Used spring boot test framework and mockito.
	
### Lombok
	
	Remove boilerplate code from simple Beans.

## Frameworks not used for simplicity and could have been used.
 
### Api Gateway

	I don't use any api gateway as there is only one call. 

### Sleught and Zipkin or another logging system

	Only one server, so there is no need of complicating thing for the logging. 

### Eureka discovery
	
	No need for this application to be called from another service so it is useless	at this point.

### Hateoas
	
	End points apart from this resource are unknown so it would be useless.

### Swagger ui
	
	Being a test it doesn't give anything valueable.
	