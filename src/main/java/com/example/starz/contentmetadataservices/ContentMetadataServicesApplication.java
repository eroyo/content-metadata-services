package com.example.starz.contentmetadataservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.example.starz.contentmetadataservices.repository.clients")
public class ContentMetadataServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContentMetadataServicesApplication.class, args);
	}
}
