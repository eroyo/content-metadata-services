package com.example.starz.contentmetadataservices.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.starz.contentmetadataservices.entities.Movies;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;
import com.example.starz.contentmetadataservices.filter.enums.FilterEnum;
import com.example.starz.contentmetadataservices.filter.factory.FilterFactory;
import com.example.starz.contentmetadataservices.services.MoviesService;

@RestController()
@RequestMapping(produces = "application/json")
public class ContentMetadataController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	private FilterFactory filterFactory;
	
	private MoviesService moviesService;
	
	public ContentMetadataController(FilterFactory filterFactory, MoviesService moviesService){
		this.filterFactory = filterFactory;
		this.moviesService = moviesService;
	}
		
	@GetMapping("/media")
	public Movies getContentMetada(@RequestParam("filter") FilterEnum filter, @RequestParam("level") FilterLevelEnum level){
		log.info("Media filter has been called with the filter :"+ filter.getValue() + " and the level: "+ level.getValue());
		return moviesService.getMoviesFilterBy(filterFactory.build(filter, level));
	}

}
