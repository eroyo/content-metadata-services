package com.example.starz.contentmetadataservices.entities;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent=false, chain=true)
@AllArgsConstructor
@NoArgsConstructor
public class Content {
	
	Long bitrate;
	Long duration;
	String format;
	Long height;
	String language;
	Long width;
	String id;
	String guid;
	List<String> assetTypeIds;
	List<String> assetTypes;
	String downloadUrl;
	List<Release> releases;
	String serverId;
	String streamingUrl;
	String protectionScheme;



}
