package com.example.starz.contentmetadataservices.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent=false, chain=true)
@AllArgsConstructor
@NoArgsConstructor
public class Entry {
	
	List<Media> media;
	
	@JsonProperty(value = "peg$contentClassification")
	String contentClassification;
	

}
