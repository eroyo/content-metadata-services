package com.example.starz.contentmetadataservices.entities;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent=false, chain=true)
@AllArgsConstructor
@NoArgsConstructor
public class Media {

	String id;
	String title;
	String guid;
	String owner;
	Long availableDate;
	Long expirationDate;
	List<Object> ratings;
	List<Content> content;
	String restrictionId;
	String availabilityState;

}
