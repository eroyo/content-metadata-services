package com.example.starz.contentmetadataservices.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent=false, chain=true)
@AllArgsConstructor
@NoArgsConstructor
public class Release {
		
	String pid;
	String url;
	String restrictionId;

}
