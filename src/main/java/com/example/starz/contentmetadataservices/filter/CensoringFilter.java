package com.example.starz.contentmetadataservices.filter;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;


public class CensoringFilter implements Function<Entry, Entry>{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private static final char CENSORED_CHARACTER = 'C';
	private FilterLevelEnum level;
	
	public CensoringFilter(FilterLevelEnum level){
		this.level = level;
	}
	

	@Override
	public Entry apply(Entry entry) {
		if ((!StringUtils.isEmpty(entry.getContentClassification()) && entry.getContentClassification().equals(FilterLevelEnum.censored.getValue()))){
			entry.setMedia(
				entry.getMedia()
					.stream()
					.filter( media -> {
						char lastCharacter = media.getGuid().charAt(media.getGuid().length()-1);
						if (level == FilterLevelEnum.uncensored){
							if (lastCharacter == CENSORED_CHARACTER){
								log.debug("Filtered with level " + level + " the media with guid " + media.getGuid());
								return false;
							}else {
								log.debug("Not filtering with level " + level + " the media with guid " + media.getGuid());
								return true;
							}
						}else {
							if (lastCharacter != CENSORED_CHARACTER){
								log.debug("Filtered with level " + level + " the media with guid " + media.getGuid());
								return false;
							}else {
								log.debug("Not filtering with level " + level + " the media with guid " + media.getGuid());
								return true;
							}
						}
					})
					.collect(Collectors.toList())
			);
		}
		return entry;
	}


	public FilterLevelEnum getLevel() {
		return level;
	}

	
	
	
}
