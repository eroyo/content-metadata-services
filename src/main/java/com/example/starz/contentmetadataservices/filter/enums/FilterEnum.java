package com.example.starz.contentmetadataservices.filter.enums;

public enum FilterEnum {

	censoring("censoring");
	
	private String value;
	
	FilterEnum(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
