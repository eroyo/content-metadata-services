package com.example.starz.contentmetadataservices.filter.enums;

public enum FilterLevelEnum {

	uncensored("Uncensored"), censored("Censored");
	
	private String value;
	
	FilterLevelEnum(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
