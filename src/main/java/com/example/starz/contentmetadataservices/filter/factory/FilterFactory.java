package com.example.starz.contentmetadataservices.filter.factory;

import java.util.function.Function;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.filter.enums.FilterEnum;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;

public interface FilterFactory {

	Function<Entry, Entry> build(FilterEnum filter, FilterLevelEnum level);
		
}
