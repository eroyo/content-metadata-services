package com.example.starz.contentmetadataservices.filter.factory;

import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.filter.CensoringFilter;
import com.example.starz.contentmetadataservices.filter.enums.FilterEnum;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;

@Component
public class FilterFactoryImpl implements FilterFactory{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());	
	
	@Override
	public Function<Entry, Entry> build(FilterEnum filter, FilterLevelEnum level){
		
		if (filter == FilterEnum.censoring){
			log.debug("Creating Censoring filter with level: " + level.getValue());
			return new CensoringFilter(level);
		}
		else {
			log.error("Filter value doesn't exist: " + filter.getValue());	
			throw new RuntimeException("Filter not implemented");
		}
		
	}

}
