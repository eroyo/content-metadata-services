package com.example.starz.contentmetadataservices.repository.clients;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.starz.contentmetadataservices.entities.Movies;

@RibbonClient
@FeignClient("movies")
@Profile("!test")
public interface MoviesClient {
	
	@GetMapping("/api/v1.0/mediaCatalog/titles/movies")
	public Movies getMovies();

}
