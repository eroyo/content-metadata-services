package com.example.starz.contentmetadataservices.services;

import java.util.function.Function;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.entities.Movies;

public interface MoviesService{
	
	Movies getMoviesFilterBy(Function<Entry, Entry> censoringFilter);

}
