package com.example.starz.contentmetadataservices.services.impl;


import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.entities.Movies;
import com.example.starz.contentmetadataservices.repository.clients.MoviesClient;
import com.example.starz.contentmetadataservices.services.MoviesService;

@Service
public class MoviesServiceImpl implements MoviesService{
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	private MoviesClient moviesClient;
	
	public MoviesServiceImpl(MoviesClient moviesClient){
		this.moviesClient = moviesClient;
	}
	
	
	private Movies getMovies() {
		return moviesClient.getMovies();
	}

	
	private Movies filter(Movies movies, Function<Entry, Entry> filter) {
		
		log.info("start filtering movies...");
		
		List<Entry> entries = movies.getEntries()
				.stream()
				.map(filter)
				.collect(Collectors.toList());
		
		log.info("ended filtering movies...");
		return new Movies(entries);
		
		
	}

	@Override
	public Movies getMoviesFilterBy(Function<Entry, Entry> filter) {
		
		return filter(getMovies(), filter);
		
	}
	

	
}
