package com.example.starz.contentmetadataservices.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;

import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import com.example.starz.contentmetadataservices.repository.clients.MoviesClient;

import feign.Client;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.Response;
import feign.Request.Options;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;

@TestConfiguration
public class ClientConfigurationTest {
	
    @Bean
    @Primary
    public MoviesClient getMoviesClient() throws IOException, URISyntaxException{
		String content = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
                .getResource("MovieClientCompleteResult.json")
                .toURI())));
		
		Client mockClient = Mockito.mock(Client.class);
		Mockito.when(mockClient.execute(Mockito.any(Request.class), Mockito.any(Options.class))).thenReturn(
				Response.builder().status(200).reason("OK").headers(Collections.<String, Collection<String>>emptyMap()).body(content, StandardCharsets.UTF_8).build());

    	
		return Feign.builder()
				.client(mockClient)
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.logger(new Slf4jLogger(MoviesClient.class))
				.contract(new SpringMvcContract())
				.logLevel(Logger.Level.FULL)
				.target(MoviesClient.class, "a");
    }

}
