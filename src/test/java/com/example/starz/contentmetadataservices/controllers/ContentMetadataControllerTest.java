package com.example.starz.contentmetadataservices.controllers;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.starz.contentmetadataservices.ContentMetadataServicesApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={ContentMetadataServicesApplication.class, ClientConfigurationTest.class})
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ContentMetadataControllerTest {
	
    private static final String UNCENSORED_LEVEL_VALUE = "uncensored";
    private static final String CENSORED_LEVEL_VALUE = "censored";
    private static final String UNKNOWN_LEVEL_VALUE = "unknown";
	private static final String UNCENSORED_RESULT = "THINKLIKEAMANY2012M";
	private static final String CENSORED_RESULT = "THINKLIKEAMANY2012MC";
	private static final String FILTER_PATH_DIFFERENCE = "$.entries[3].media[0].guid";
	private static final String API_ENDPOINT = "/media?filter=censoring&level=";
	private static final String API_ENDPOINT_UNKWON_FILTER = "/media?filter=unknown&level=";
	@Autowired
    private MockMvc mockMvc;
    

    @Test
    public void mediaServiceController_uncensoredFilter() throws Exception {
        this.mockMvc.perform(get(API_ENDPOINT+UNCENSORED_LEVEL_VALUE)).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath(FILTER_PATH_DIFFERENCE).value(UNCENSORED_RESULT));
    }
    
    @Test
    public void mediaServiceController_censoredFilter() throws Exception {
        this.mockMvc.perform(get(API_ENDPOINT+CENSORED_LEVEL_VALUE)).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath(FILTER_PATH_DIFFERENCE).value(CENSORED_RESULT));
    }
    
    @Test
    public void mediaServiceController_unknownLevelFilter() throws Exception {
        this.mockMvc.perform(get(API_ENDPOINT+UNKNOWN_LEVEL_VALUE)).andDo(print()).andExpect(status().is(400));
    }
    
    @Test
    public void mediaServiceController_unknownFilter() throws Exception {
        this.mockMvc.perform(get(API_ENDPOINT_UNKWON_FILTER+CENSORED_RESULT)).andDo(print()).andExpect(status().is(400));
    }
    
}
