package com.example.starz.contentmetadataservices.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;
import com.example.starz.contentmetadataservices.utils.MediaTestUtil;

public class CensoringFilterTest {
	
	private static final String UNCENSORED_GUID = "filter";
	private static final String CENSORED_GUID = "filterC";

	@Test
	public void CensoringFilter_FilterWithClassificationUncensoredAndWithoutLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.uncensored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(null);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.uncensored.getValue(), entryResult.getContentClassification());
		assertEquals(2, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(1).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithClassificationCensoredAndWithoutLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.censored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(null);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.censored.getValue(), entryResult.getContentClassification());
		assertEquals(1, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithEmptyClassificationAndWithoutLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification("").setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(null);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals("", entryResult.getContentClassification());
		assertEquals(2, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(1).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithoutClassificationAndWithoutLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification("").setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(null);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals("", entryResult.getContentClassification());
		assertEquals(2, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(1).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithClassificationUncensoredAndUncensoredLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.uncensored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(FilterLevelEnum.uncensored);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.uncensored.getValue(), entryResult.getContentClassification());
		assertEquals(2, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(1).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithClassificationUncensoredAndCensoredLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.uncensored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(FilterLevelEnum.censored);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.uncensored.getValue(), entryResult.getContentClassification());
		assertEquals(2, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(1).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithClassificationCensoredAndUncensoredLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.censored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(FilterLevelEnum.uncensored);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.censored.getValue(), entryResult.getContentClassification());
		assertEquals(1, entryResult.getMedia().size());
		assertEquals(UNCENSORED_GUID, entryResult.getMedia().get(0).getGuid());
	}
	
	@Test
	public void CensoringFilter_FilterWithClassificationCensoredAndCensoredLevel(){
		
		//given
		Entry entry = new Entry().setContentClassification(FilterLevelEnum.censored.getValue()).setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID));
		
		//when
		CensoringFilter censoringFilter = new CensoringFilter(FilterLevelEnum.censored);
		Entry entryResult = censoringFilter.apply(entry);
		
		//then
		assertEquals(FilterLevelEnum.censored.getValue(), entryResult.getContentClassification());
		assertEquals(1, entryResult.getMedia().size());
		assertEquals(CENSORED_GUID, entryResult.getMedia().get(0).getGuid());
	}

}
