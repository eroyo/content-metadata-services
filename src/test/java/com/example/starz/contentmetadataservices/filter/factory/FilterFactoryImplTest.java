package com.example.starz.contentmetadataservices.filter.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.function.Function;

import org.junit.Test;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.filter.CensoringFilter;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;
import com.example.starz.contentmetadataservices.filter.enums.FilterEnum;

public class FilterFactoryImplTest {
	
	@Test
	public void FactoryBuild_CensoringFilterTest(){
		
		FilterFactory filterFactory = new FilterFactoryImpl();
		Function<Entry, Entry> filter = filterFactory.build(FilterEnum.censoring, FilterLevelEnum.censored);
		assertNotNull(filter);
		assertTrue(filter instanceof CensoringFilter);
		assertEquals(FilterLevelEnum.censored, ((CensoringFilter) filter).getLevel());
		
	}
	
	@Test
	public void FactoryBuild_UncensoringFilterTest(){
		
		FilterFactory filterFactory = new FilterFactoryImpl();
		Function<Entry, Entry> filter = filterFactory.build(FilterEnum.censoring, FilterLevelEnum.uncensored);
		assertNotNull(filter);
		assertTrue(filter instanceof CensoringFilter);
		assertEquals(FilterLevelEnum.uncensored, ((CensoringFilter) filter).getLevel());
		
	}

}
