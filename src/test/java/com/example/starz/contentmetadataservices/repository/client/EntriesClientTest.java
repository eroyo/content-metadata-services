package com.example.starz.contentmetadataservices.repository.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.starz.contentmetadataservices.entities.Content;
import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.entities.Media;
import com.example.starz.contentmetadataservices.entities.Movies;
import com.example.starz.contentmetadataservices.entities.Release;
import com.example.starz.contentmetadataservices.repository.clients.MoviesClient;

import feign.Client;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.Request.Options;
import feign.Response;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;

@RunWith(SpringJUnit4ClassRunner.class)
public class EntriesClientTest {
	
	private static final String MOCKED_ENDPOINT = "a";
	private static final String MOVIE_CLIENT_SIMPLE_RESULT_JSON = "MovieClientSimpleResult.json";
	MoviesClient moviesClient;
	
	@Before
	public void setup() throws IOException, URISyntaxException {

		String content = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
                .getResource(MOVIE_CLIENT_SIMPLE_RESULT_JSON)
                .toURI())));
		
		Client mockClient = Mockito.mock(Client.class);
		Mockito.when(mockClient.execute(Mockito.any(Request.class), Mockito.any(Options.class))).thenReturn(
				Response.builder().status(200).reason("OK").headers(Collections.<String, Collection<String>>emptyMap()).body(content, StandardCharsets.UTF_8).build());

		moviesClient = Feign.builder()
			.client(mockClient)
			.encoder(new JacksonEncoder())
			.decoder(new JacksonDecoder())
			.logger(new Slf4jLogger(MoviesClient.class))
			.contract(new SpringMvcContract())
			.logLevel(Logger.Level.FULL)
			.target(MoviesClient.class, MOCKED_ENDPOINT);
	  }

	@Test
	public void EntriesClientCallTest(){
		
		Movies movies = moviesClient.getMovies();
		assertNotNull(movies);
		
		List<String> assertTypeIds = new ArrayList<>();
		assertTypeIds.add("http://data.media.theplatform.eu/media/data/AssetType/32170565115");
		assertTypeIds.add("http://data.media.theplatform.eu/media/data/AssetType/33266757377");
		
		List<String> assertTypes = new ArrayList<>();
		assertTypes.add("hls_primetime_aaxs_acloud");
		assertTypes.add("french");
		
		List<Release> releases = new ArrayList<>();
		releases.add(new Release().setPid("p1znofE8h6qx")
				.setUrl("http://link.theplatform.eu/s/hGKjSC/p1znofE8h6qx")
				.setRestrictionId(""));
		
		List<Content> contents = new ArrayList<>();
		contents.add(new Content()
			.setBitrate(4623671L)
			.setDuration(8372L)
			.setFormat("M3U")
			.setHeight(0L)
			.setLanguage("")
			.setWidth(0L)
			.setId("http://data.media.theplatform.eu/media/data/MediaFile/62820933501")
			.setGuid("RZwdaJ2aSjAHvJU3ajpLfJpWRb35A_Fb")
			.setAssetTypeIds(assertTypeIds)
			.setAssetTypes(assertTypes)
			.setDownloadUrl("")
			.setReleases(releases)
			.setServerId("http://data.media.theplatform.eu/media/data/Server/3058245282")
			.setStreamingUrl("http://dev-cdn-lb.parsifalentertainment.com/CP04/OZTHEGREATANDPOWERFULY2013MFR/OZTHEGREATANDPOWERFULY2013MFR.m3u8?faxs=1")
			.setProtectionScheme("flashAccess"));
		
		List<Entry> entries = movies.getEntries();
		assertEquals(1, entries.size());
		assertEntry(entries.get(0), "expected", new Media()
				.setId("http://data.media.theplatform.eu/media/data/Media/62819397370")
				.setTitle("Oz : The Great And Powerful")
				.setGuid("OZTHEGREATANDPOWERFULY2013MFR")
				.setOwner(null)
				.setAvailableDate(1426377600000L)
				.setExpirationDate(1568419200000L)
				.setRatings(new ArrayList<>())
				.setContent(contents)
				.setRestrictionId("")
				.setAvailabilityState("available"));
	}

	private void assertEntry(Entry entry, String contentClasification,
			Media media) {
		assertEquals(1, entry.getMedia().size());
		assertEquals(media,entry.getMedia().get(0));
		assertEquals(contentClasification, entry.getContentClassification());
		
	}



}
