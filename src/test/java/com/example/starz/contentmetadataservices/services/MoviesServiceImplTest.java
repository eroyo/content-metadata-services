package com.example.starz.contentmetadataservices.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.starz.contentmetadataservices.entities.Entry;
import com.example.starz.contentmetadataservices.entities.Media;
import com.example.starz.contentmetadataservices.entities.Movies;
import com.example.starz.contentmetadataservices.filter.CensoringFilter;
import com.example.starz.contentmetadataservices.filter.enums.FilterLevelEnum;
import com.example.starz.contentmetadataservices.repository.clients.MoviesClient;
import com.example.starz.contentmetadataservices.services.impl.MoviesServiceImpl;
import com.example.starz.contentmetadataservices.utils.MediaTestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
public class MoviesServiceImplTest {

	private static final String UNCENSORED_GUID = "mock2";
	private static final String CENSORED_GUID = "mockC";


	@Test
	public void filterMovies_byCensoredTest(){
		
		List<Entry> entries = givenEntriesOfAllTypesOfCensoring();
		List<Entry> expectedResult = cloneInitialMediasForResult(entries);

		Movies returnMovies = whenFilteringByCensoring(entries, FilterLevelEnum.censored);
		
		//then
		List<Entry> returnEntries = returnMovies.getEntries();
		assertEquals(4, returnEntries.size());
		assertEntry(returnEntries.get(0), getCensoredMedia(expectedResult, 0));
		assertEntry(returnEntries.get(2), getCensoredMedia(expectedResult, 2), getUnCensoredMedia(expectedResult, 2));
		assertEntry(returnEntries.get(2), getCensoredMedia(expectedResult, 2), getUnCensoredMedia(expectedResult, 2));
		assertEntry(returnEntries.get(3), getCensoredMedia(expectedResult, 3), getUnCensoredMedia(expectedResult, 3));
		
	}
	
	private List<Entry> givenEntriesOfAllTypesOfCensoring() {
		List<Entry> entries = new ArrayList<>();	
		entries = generateEntryWithOneMediaCensoredAndOneNot(entries, FilterLevelEnum.censored.getValue());
		entries = generateEntryWithOneMediaCensoredAndOneNot(entries, FilterLevelEnum.uncensored.getValue());
		entries = generateEntryWithOneMediaCensoredAndOneNot(entries, "");
		entries = generateEntryWithOneMediaCensoredAndOneNot(entries, null);
		return entries;
	}
	
	private List<Entry> generateEntryWithOneMediaCensoredAndOneNot(List<Entry> entries, String contentClassification) {
		entries.add(new Entry().setMedia(MediaTestUtil.buildMediaList(CENSORED_GUID, UNCENSORED_GUID)).setContentClassification(contentClassification));
		return entries;
	}

	private List<Entry> cloneInitialMediasForResult(List<Entry> entries) {
		List<Entry> clone = new ArrayList<Entry>();
		for (Entry entry : entries){
			List<Media> mediaClone = new ArrayList<Media>(entry.getMedia());
			clone.add(new Entry().setMedia(mediaClone));
		}
		return clone;
	}

	private Movies whenFilteringByCensoring(List<Entry> entries, FilterLevelEnum censoringLevel) {	
		Movies movies = mockMovies(entries);	
		MoviesClient moviesClient = mockMoviesClient(movies);
		MoviesServiceImpl entriesServiceImpl = new MoviesServiceImpl(moviesClient);
		CensoringFilter censoringFilter = new CensoringFilter(censoringLevel); 
		Movies returnMovies = entriesServiceImpl.getMoviesFilterBy(censoringFilter);
		return returnMovies;
	}



	private Media getCensoredMedia(List<Entry> entries, int entryPosition) {
		return entries.get(entryPosition).getMedia().get(0);
	}
	
	private Media getUnCensoredMedia(List<Entry> entries, int entryPosition) {
		return entries.get(entryPosition).getMedia().get(1);
	}


	
	private void assertEntry(Entry entry, Media... medias) {

		List<Media> returnMedias = entry.getMedia();
		List<Media> copyReturnMedias = new ArrayList<>(returnMedias);
		for (Media media : medias){
			
			assertTrue(returnMedias.contains(media));
			copyReturnMedias.remove(media);
			
		}
		assertTrue(copyReturnMedias.isEmpty());
		
	}
	

	private MoviesClient mockMoviesClient(Movies movies) {
		MoviesClient moviesClient = mock(MoviesClient.class);
		when(moviesClient.getMovies()).thenReturn(movies);
		return moviesClient;
	}

	private Movies mockMovies(List<Entry> entries) {
		Movies movies = mock(Movies.class);
		when(movies.getEntries()).thenReturn(entries);
		return movies;
	}


	@Test
	public void filterMovies_byUnCensoredTest(){
		
		List<Entry> entries = givenEntriesOfAllTypesOfCensoring();
		List<Entry> expectedResult = cloneInitialMediasForResult(entries);

		Movies returnMovies = whenFilteringByCensoring(entries, FilterLevelEnum.uncensored);
		
		//then
		List<Entry> returnEntries = returnMovies.getEntries();
		assertEquals(4, returnEntries.size());
		assertEntry(returnEntries.get(0), getUnCensoredMedia(expectedResult, 0));
		assertEntry(returnEntries.get(1), getCensoredMedia(expectedResult, 1), getUnCensoredMedia(expectedResult, 1));
		assertEntry(returnEntries.get(2), getCensoredMedia(expectedResult, 2), getUnCensoredMedia(expectedResult, 2));
		assertEntry(returnEntries.get(3), getCensoredMedia(expectedResult, 3), getUnCensoredMedia(expectedResult, 3));
		
	}


	
}


