package com.example.starz.contentmetadataservices.utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.example.starz.contentmetadataservices.entities.Media;

public class MediaTestUtil {
	
	public static List<Media> buildMediaList(String... guids){
		List<Media> mediaList = new ArrayList<>();
		for(String guid : guids){
			Media media1 = mockMedia(guid);
			mediaList.add(media1);
		}
		return mediaList;
	}
	
	private static Media mockMedia(String guid) {
		Media media = mock(Media.class);
		when(media.getGuid()).thenReturn(guid);
		return media;
	}

}
